﻿using mBank.ClientApp.Services.DTO;
using mBank.ClientApp.Services.Services;
using Microsoft.Win32;
using System;
using System.IO;
using System.Text;
using System.Windows;

namespace mBank.ClientApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ImportService _importService;

        public MainWindow()
        {
            InitializeComponent();
            _importService = new ImportService();
        }

        private async void LoadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TransactionImportDto dataToImport = new TransactionImportDto();
                LoadFile(dataToImport);

                var response = await _importService.Import(dataToImport);
                Results.Items.Add(response);
            }
            catch (Exception ex)
            {
                Results.Items.Add("Błąd odczytu pliku: " + ex.Message);
            }
        }

        private void LoadFile(TransactionImportDto dataToImport)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".TRA";

            if (openFileDialog.ShowDialog() == true)
            {
                dataToImport.FileName = openFileDialog.SafeFileName;

                var fileStream = openFileDialog.OpenFile();
                ReadFile(fileStream, dataToImport);
            }
        }

        private void ReadFile(Stream fileStream, TransactionImportDto dataToImport)
        {
            using (StreamReader reader = new StreamReader(fileStream, Encoding.GetEncoding(1250)))
            {
                var fileContent = reader.ReadToEnd();
                dataToImport.File = System.Text.Encoding.UTF8.GetBytes(fileContent);
            }
        }
    }
}
