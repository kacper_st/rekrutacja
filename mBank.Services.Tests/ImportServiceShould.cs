using AutoMapper;
using mBank.Data.Entities;
using mBank.Repositories;
using mBank.Repositories.Repositories;
using mBank.Services.DTO;
using mBank.Services.Services;
using Moq;
using System;
using System.Text;
using Xunit;

namespace mBank.Services.Tests
{
    public class ImportServiceShould
    {
        private Mock<ITransactionInputRepository> _mockTransactionInputRepo;
        private Mock<IEntityRepository<Transaction>> _mockTransactionRepo;
        private Mock<IMapper> _mockAutoMapper;

        public ImportServiceShould()
        {
            _mockTransactionInputRepo = new Mock<ITransactionInputRepository>();
            _mockTransactionRepo = new Mock<IEntityRepository<Transaction>>();
            _mockAutoMapper = new Mock<IMapper>();
        }

        [Theory]
        [InlineData(true, true)]
        [InlineData(false, false)]
        public async void CheckIfTransactionFileIsAlreadyImported(bool setupValue, bool expectedValue)
        {
            string fileName = "test";

            _mockTransactionInputRepo.Setup(x => x.CheckIfAlreadyImported(fileName)).ReturnsAsync(setupValue);
            ImportService sut = new ImportService(_mockTransactionInputRepo.Object, _mockTransactionRepo.Object, _mockAutoMapper.Object);

            var result = await sut.CheckIfAlreadyImported(fileName);

            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public async void ThrowAnExceptionIfValidationFails()
        {
            FileDto dto = new FileDto { FileName = "name", File = Encoding.UTF8.GetBytes("\"some\",\"failed\",\"validations\"") };
            ImportService sut = new ImportService(_mockTransactionInputRepo.Object, _mockTransactionRepo.Object, _mockAutoMapper.Object);

            await Assert.ThrowsAnyAsync<Exception>(() => sut.ImportData(dto));
        }
    }
}
