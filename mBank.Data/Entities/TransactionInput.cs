﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace mBank.Data.Entities
{
    public class TransactionInput : IEntity
    {
        [Key]
        public int Id { get; set; }

        public DateTime ImportDate { get; set; }
        
        [Required]
        public string FileName { get; set; }

        public virtual IEnumerable<Transaction> Transactions { get; set; }
    }
}
