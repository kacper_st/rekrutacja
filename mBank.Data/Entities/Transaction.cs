﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mBank.Data.Entities
{
    public class Transaction : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int TransactionInputId { get; set; }

        [StringLength(15)]
        public string Amount { get; set; }

        public DateTime AccountChargeDate { get; set; }

        [StringLength(34)]
        public string RecipentAccountNumber { get; set; }

        public string TransactionBody { get; set; }

        public virtual TransactionInput TransactionInput { get; set; }
    }
}
