﻿using mBank.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace mBank.Data
{
    public class mBankDbContext: DbContext
    {
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<TransactionInput> TransactionInputs { get; set; }

        public mBankDbContext(DbContextOptions<mBankDbContext> options) : base(options)
        {

        }
    }
}
