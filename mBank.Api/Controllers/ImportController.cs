﻿using mBank.Services.DTO;
using mBank.Services.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace mBank.Api.Controllers
{
    [ApiController]
    public class ImportController : ControllerBase
    {
        private IImportService _importService;

        public ImportController(IImportService importService)
        {
            _importService = importService;
        }

        [HttpPost("api/import")]
        public async Task<ActionResult> ImportFile([FromBody] FileDto fileDto)
        {
            try
            {
                var isAlreadyImported = await _importService.CheckIfAlreadyImported(fileDto.FileName);

                if (isAlreadyImported)
                {
                    return BadRequest("File " + fileDto.FileName + " is already imported");
                }

                await _importService.ImportData(fileDto);
                return Ok(fileDto.FileName + " imported successfully");
            }
            catch (Exception ex)
            {
                return BadRequest("Validation of " + fileDto.FileName + " has failed with message: " + ex.Message);
            }
        }
    }
}
