﻿using AutoMapper;
using mBank.Data.Entities;
using mBank.Services.DTO;

namespace mBank.Api
{
    public class AutoMapperDefaultProfile : Profile
    {
        public AutoMapperDefaultProfile()
        {
            CreateMap<TransactionDto, Transaction>().ReverseMap();
            CreateMap<TransactionInputDto, TransactionInput>().ReverseMap();
            CreateMap<TransactionFileDto, TransactionDto>()
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Kwota))
                .ForMember(dest => dest.AccountChargeDate, opt => opt.MapFrom(src => src.DataObciazeniaRachunku))
                .ForMember(dest => dest.RecipentAccountNumber, opt => opt.MapFrom(src => src.RachunekKlientaAdresata))
                .ForMember(dest => dest.TransactionBody, opt => opt.MapFrom(src => src.Transakcja));
        }
    }
}
