﻿namespace mBank.ClientApp.Services.DTO
{
    public class TransactionImportDto
    {
        public string FileName { get; set; }
        public byte[] File { get; set; }
    }
}
