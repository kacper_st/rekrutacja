﻿using mBank.ClientApp.Services.DTO;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace mBank.ClientApp.Services.Services
{
    public class ImportService
    {
        private const string _apiUrl = "https://localhost:44353";

        public async Task<string> Import(TransactionImportDto transactionData)
        {
            HttpClient _client;
            HttpResponseMessage response;

            using (_client = new HttpClient(new HttpClientHandler { UseDefaultCredentials = true }))
            {
                try
                {
                    string endpoint = _apiUrl + "/api/import";
                    var serializedTransactionData = JsonConvert.SerializeObject(transactionData);
                    var content = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(serializedTransactionData));
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    response = await _client.PostAsync(endpoint, content);
                }
                catch(Exception ex)
                {
                    return "An error occured while sending the file " + ex.Message;
                }
            }

            return await response.Content.ReadAsStringAsync();
        }
    }
}
