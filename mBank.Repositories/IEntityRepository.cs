﻿using mBank.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mBank.Repositories
{
    public interface IEntityRepository<TEntity> where TEntity : IEntity
    {
        Task<TEntity> Get(int id);
        Task SaveChanges();
        Task<TEntity> Add(TEntity entity, bool saveChanges = true);
        Task AddRange(IEnumerable<TEntity> entities, bool saveChanges = true);
    }
}
