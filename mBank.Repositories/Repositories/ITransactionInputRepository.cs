﻿using mBank.Data.Entities;
using System.Threading.Tasks;

namespace mBank.Repositories.Repositories
{
    public interface ITransactionInputRepository : IEntityRepository<TransactionInput>
    {
       Task<bool> CheckIfAlreadyImported(string fileName);
    }
}
