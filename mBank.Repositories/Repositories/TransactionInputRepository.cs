﻿using mBank.Data;
using mBank.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace mBank.Repositories.Repositories
{
    public class TransactionInputRepository : EntityRepository<TransactionInput>, ITransactionInputRepository
    {
        public TransactionInputRepository(mBankDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<bool> CheckIfAlreadyImported(string fileName)
        {
           return await _dbContext.TransactionInputs.AnyAsync(x => x.FileName == fileName);
        }
    }
}
