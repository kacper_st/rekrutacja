﻿using mBank.Data;
using mBank.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mBank.Repositories
{
    public class EntityRepository<TEntity> : IEntityRepository<TEntity> where TEntity : class, IEntity
    {
        protected mBankDbContext _dbContext;

        public EntityRepository(mBankDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TEntity> Add(TEntity entity, bool saveChanges = true)
        {
            var result = await _dbContext.Set<TEntity>().AddAsync(entity);

            if (saveChanges)
            {
                await _dbContext.SaveChangesAsync();
            }

            return result.Entity;
        }

        public async Task AddRange(IEnumerable<TEntity> entities, bool saveChanges = true)
        {
            await _dbContext.Set<TEntity>().AddRangeAsync(entities);

            if (saveChanges)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task<TEntity> Get(int id)
        {
            return await _dbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task SaveChanges()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
