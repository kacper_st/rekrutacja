using mBank.Api.Controllers;
using mBank.Services.DTO;
using mBank.Services.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Text;
using System.Web.Mvc;
using Xunit;

namespace mBank.Api.Tests
{
    public class ImportControllerShould
    {
        private Mock<IImportService> _mockImportService;

        public ImportControllerShould()
        {
            _mockImportService = new Mock<IImportService>();
        }

        [Fact]
        public async void ReturnBadRequestIfFileIsAlreadyImported()
        {
            FileDto dto = new FileDto { FileName = "name" };
            _mockImportService.Setup(x => x.CheckIfAlreadyImported(dto.FileName)).ReturnsAsync(true);

            ImportController sut = new ImportController(_mockImportService.Object);

            var result = await sut.ImportFile(dto) as StatusCodeResult;

            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void ReturnBadRequestIfValidationFailed()
        {
            FileDto dto = new FileDto { FileName = "name" , File = Encoding.UTF8.GetBytes("\"some\",\"failed\",\"validations\"") };
            _mockImportService.Setup(x => x.CheckIfAlreadyImported(dto.FileName)).ReturnsAsync(false);

            ImportController sut = new ImportController(_mockImportService.Object);

            var result = sut.ImportFile(dto);

            //Assert.Equal(400, sut.Result.StatusCode);
        }
    }
}
