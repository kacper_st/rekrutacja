﻿using FluentValidation;
using mBank.Services.DTO;

namespace mBank.Services.Validators
{
    public class TransactionFileValidator : AbstractValidator<TransactionFileDto>
    {
        public TransactionFileValidator()
        {
            this.RuleFor(x => x.TypKomunikatu).NotNull().Equals("110");
            this.RuleFor(x => x.DataObciazeniaRachunku).NotNull();
            this.RuleFor(x => x.Kwota).NotNull().MaximumLength(15).Must(x => IsDigitsOnly(x));
            this.RuleFor(x => x.NrJednostkiPrezentujacej).NotNull().MaximumLength(8).Must(x => IsDigitsOnly(x));
            this.RuleFor(x => x.NrJednostkiOdbierajacej).NotNull().MaximumLength(8).Must(x => IsDigitsOnly(x)); 
            this.RuleFor(x => x.RachunekKlientaNadawcy).MaximumLength(34);
            this.RuleFor(x => x.RachunekKlientaAdresata).MaximumLength(34);
            this.RuleFor(x => x.NazwaKlientaNadawcy).MaximumLength(144); 
            this.RuleFor(x => x.NazwaKlientaAdresata).MaximumLength(144);
            this.RuleFor(x => x.NrNadawcy).MaximumLength(8).Must(x => IsDigitsOnly(x)); 
            this.RuleFor(x => x.NrOddzialu).MaximumLength(8).Must(x => IsDigitsOnly(x));
            this.RuleFor(x => x.InformacjeDodatkowe);
            this.RuleFor(x => x.NrCzeku).Empty().MaximumLength(10);
            this.RuleFor(x => x.SzczegolyReklamacji).Empty().MaximumLength(144); 
            this.RuleFor(x => x.DodatkowaIdentyfikacjaSpraw).MaximumLength(144); 
            this.RuleFor(x => x.InformacjeMiedzybankowe).MaximumLength(216); 
        }

        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                {
                    return false;
                }
            }

            return true;
        }
    }
}
