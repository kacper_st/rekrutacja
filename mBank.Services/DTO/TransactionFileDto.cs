﻿using System;

namespace mBank.Services.DTO
{
    public class TransactionFileDto
    {
        public string TypKomunikatu { get; set; }
        public DateTime? DataObciazeniaRachunku { get; set; }
        public string Kwota { get; set; }
        public string NrJednostkiPrezentujacej { get; set; }
        public string NrJednostkiOdbierajacej { get; set; }
        public string RachunekKlientaNadawcy { get; set; }
        public string RachunekKlientaAdresata { get; set; }
        public string NazwaKlientaNadawcy { get; set; }
        public string NazwaKlientaAdresata { get; set; }
        public string NrNadawcy { get; set; }
        public string NrOddzialu { get; set; }
        public string InformacjeDodatkowe { get; set; }
        public string NrCzeku { get; set; }
        public string SzczegolyReklamacji { get; set; }
        public string DodatkowaIdentyfikacjaSpraw { get; set; }
        public string InformacjeMiedzybankowe { get; set; }
        public string Transakcja { get; set; }
    }
}
