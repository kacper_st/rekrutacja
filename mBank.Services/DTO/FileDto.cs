﻿namespace mBank.Services.DTO
{
    public class FileDto
    {
        public string FileName { get; set; }
        public byte[] File { get; set; }
    }
}
