﻿using System;

namespace mBank.Services.DTO
{
    public class TransactionInputDto
    {
        public int Id { get; set; }
        public DateTime ImportDate { get; set; }
        public string FileName { get; set; }
    }
}
