﻿using System;

namespace mBank.Services.DTO
{
    public class TransactionDto
    {
        public int Id { get; set; }
        public int TransactionInputId { get; set; }
        public string Amount { get; set; }
        public DateTime AccountChargeDate { get; set; }
        public string RecipentAccountNumber { get; set; }
        public string TransactionBody { get; set; }
    }
}
