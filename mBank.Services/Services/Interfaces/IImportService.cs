﻿using mBank.Services.DTO;
using System.Threading.Tasks;

namespace mBank.Services.Services.Interfaces
{
    public interface IImportService
    {
        Task<bool> CheckIfAlreadyImported(string fileName);
        Task ImportData(FileDto dto);
    }
}
