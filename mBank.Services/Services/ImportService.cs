﻿using AutoMapper;
using mBank.Data.Entities;
using mBank.Repositories;
using mBank.Repositories.Repositories;
using mBank.Services.DTO;
using mBank.Services.Services.Interfaces;
using mBank.Services.Validators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mBank.Services.Services
{
    public class ImportService : IImportService
    {
        private ITransactionInputRepository _transactionInputRepository;
        private IEntityRepository<Transaction> _transactionRepository;
        private IMapper _mapper;

        public ImportService(
            ITransactionInputRepository transactionInputRepository,
            IEntityRepository<Transaction> transactionRepository,
            IMapper mapper)
        {
            _transactionInputRepository = transactionInputRepository;
            _transactionRepository = transactionRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfAlreadyImported(string fileName)
        {
            return await _transactionInputRepository.CheckIfAlreadyImported(fileName);
        }

        public async Task ImportData(FileDto dto)
        {
            var transactionDtos = ValidateTransactions(dto);

            var transactionInputId = await GenerateNewTransactionInput(dto.FileName);
            await GenerateNewTransaction(transactionDtos, transactionInputId);
        }

        private async Task<int> GenerateNewTransactionInput(string fileName, bool saveChanges = true)
        {
            var transactionInput = new TransactionInput
            {
                FileName = fileName,
                ImportDate = DateTime.UtcNow
            };

            var newTransactionInput = await _transactionInputRepository.Add(transactionInput, saveChanges);
            return newTransactionInput.Id;
        }

        private async Task GenerateNewTransaction(IEnumerable<TransactionDto> transactionDtos, int transactionInputId, bool saveChanges = true)
        {
            foreach(var transactionDto in transactionDtos)
            {
                transactionDto.TransactionInputId = transactionInputId;
            }

            var transaction = _mapper.Map<IEnumerable<Transaction>>(transactionDtos);
            await _transactionRepository.AddRange(transaction, saveChanges);
        }

        private IEnumerable<TransactionDto> ValidateTransactions(FileDto dto)
        {
            var records = SplitFileToRecords(dto);
            var mappedDtos = MapRecordsToDtos(records);

            var validationResult = ValidateFieldRules(mappedDtos);
            if (!validationResult)
            {
                throw new Exception("Incorrect data in one or more records");
            }

            return _mapper.Map<IEnumerable<TransactionDto>>(mappedDtos);
        }

        private bool ValidateFieldRules(IEnumerable<TransactionFileDto> mappedDtos)
        {
            var validator = new TransactionFileValidator();

            foreach (var dto in mappedDtos)
            {
                if (!validator.Validate(dto).IsValid)
                {
                    return false;
                }
            }

            return true;
        }

        private string[] SplitFileToRecords(FileDto dto)
        {
            var stringContent = System.Text.Encoding.UTF8.GetString(dto.File);
            return stringContent.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        }

        private IEnumerable<TransactionFileDto> MapRecordsToDtos(string[] records)
        {
            List<TransactionFileDto> result = new List<TransactionFileDto>();

            foreach (var record in records)
            {
                if (record.Length > 0)
                {
                    var mappedRecord = MapSingleRecordToDto(record);
                    result.Add(mappedRecord);
                }
            }

            return result;
        }

        private TransactionFileDto MapSingleRecordToDto(string record)
        {
            var substrings = record.Split(',');
            object[] properties = new object[substrings.Length];

            for (int i = 0; i < substrings.Length; i++)
            {
                if (i == 1)
                {
                    DateTime date;
                    var dateTimeParseSuccess = DateTime.TryParseExact(substrings[i], "yyyyMMdd",
                        null, System.Globalization.DateTimeStyles.None, out date);

                    if (dateTimeParseSuccess)
                    {
                        properties[i] = date;
                        continue;
                    }
                }

                if (substrings[i].StartsWith("\""))
                {
                    var cleanSubstring = substrings[i].Remove(0, 1).Remove(substrings[i].Length - 2, 1);
                    properties[i] = (cleanSubstring);
                    continue;
                }

                properties[i] = substrings[i];
            }

            return MapToProperties(properties, record);
        }

        private TransactionFileDto MapToProperties(object[] properties, string record)
        {
            return new TransactionFileDto
            {
                TypKomunikatu = properties[0].ToString(),
                DataObciazeniaRachunku = (DateTime?)properties[1],
                Kwota = properties[2].ToString(),
                NrJednostkiPrezentujacej = properties[3].ToString(),
                NrJednostkiOdbierajacej = properties[4].ToString(),
                RachunekKlientaNadawcy = properties[5].ToString(),
                RachunekKlientaAdresata = (string)properties[6],
                NazwaKlientaNadawcy = (string)properties[7],
                NazwaKlientaAdresata = (string)properties[8],
                NrNadawcy = properties[9].ToString(),
                NrOddzialu = properties[10].ToString(),
                InformacjeDodatkowe = (string)properties[11],
                NrCzeku = (string)properties[12],
                SzczegolyReklamacji = (string)properties[13],
                DodatkowaIdentyfikacjaSpraw = (string)properties[14],
                InformacjeMiedzybankowe = (string)properties[15],
                Transakcja = record
            };
        }
    }
}
